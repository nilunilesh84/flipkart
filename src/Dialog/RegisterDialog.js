import React, { useState } from "react";
import { Dialog, Typography, withStyles } from "@material-ui/core";
import { Register } from "pages";
import { Link } from "react-router-dom";
import Login from "pages/login";

const styles = {
  dialogPaper: {
    width: "50%",
    height: "95%",
    marginTop: "12%",
    borderRadius: 0,
    boxShadow: "none",
    maxHeight: "100%",
    maxWidth: "100%",
    overflow: "hidden",
  },
};

function RegisterDialog({ classes }) {
  const [open, setOpen] = useState(true);
  const [login, setLogin] = useState(false);
  const handleClose = () => {
    setOpen(false);
    setLogin(false);
  };
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        classes={{ paper: classes.dialogPaper }}
        BackdropProps={{ style: { backgroundColor: "unset" } }}
      >
        <Register setLogin={setLogin} />
        <Typography variant='h4' color='secondary' onClick={() => setLogin(true)}>
          Existing User ? Log in
        </Typography>
      </Dialog>
      <Dialog
        open={login}
        onClose={handleClose}
        classes={{ paper: classes.dialogPaper }}
        BackdropProps={{ style: { backgroundColor: "unset" } }}
      >
        <Login  />
        {/* <Typography>Existing User ? Log in</Typography> */}
      </Dialog>
    </div>
  );
}

export default withStyles(styles)(RegisterDialog);
