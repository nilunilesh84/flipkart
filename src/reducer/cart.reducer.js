const initState = {
  cart: [],
};
export default function CartReducer(state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case "GET_ITEMS":
      return { ...state, cart: payload };
    default:
      return state;
  }
}
