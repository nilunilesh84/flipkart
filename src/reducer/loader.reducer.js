import { HIDE_LOADER, SHOW_LOADER } from "action/loader.action";

const initstate = { loading: false };

export default function loaderReducer(state = initstate, action) {
  const { type } = action;
  switch (type) {
    case SHOW_LOADER:
      return { loading: true };
    case HIDE_LOADER:
      return { loading: false };
    default:
      return state;
  }
}
