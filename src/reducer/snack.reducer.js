import { HIDE_SNACK, SHOW_SNACK } from "action/snack.action";

const initstate = {
  open: false,
  severity: "success",
  text: "",
};

export default function snackReducer(state = initstate, action) {
  const { type, payload } = action;
  switch (type) {
    case SHOW_SNACK:
      return { open: true, ...payload };
    case HIDE_SNACK:
      return { open: false, severity: "success", text: "" };
    default:
      return state;
  }
}
