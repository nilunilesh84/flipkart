const initState = {
  user: null,
  loggedIn: false,
  loginuser:null
};

export default function SessionReducer(state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case "LOG_OUT":
      return { user: null, loginuser: null };
    case 'LOGINUSER'  : return {...state,loginuser:payload}
    default:
      return state;
  }
}
