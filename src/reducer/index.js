import { combineReducers } from "redux";
import cartReducer from "./cart.reducer";
import sessionReducer from "./session.reducer";
import productReducer from "./product.reducer";
import loaderReducer from "./loader.reducer";
import snackReducer from "./snack.reducer";

const reducer = combineReducers({
  cart: cartReducer,
  session: sessionReducer,
  product: productReducer,
  loading: loaderReducer,
  snack: snackReducer
});

export default reducer;
