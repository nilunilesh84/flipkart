const initState = {
  data: [],
  singleproduct: [],
};

export default function Product(state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case "API_PRODUCTS":
      return { data: payload };
    case "PRODUCT_DETAILS":
      return { ...state, singleproduct:[payload] };
    default:
      return state;
  }
}
