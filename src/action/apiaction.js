import axios from "axios";
const PORT = "https://idgxtkil7f.execute-api.ap-south-1.amazonaws.com/dev";

export const addUsers = (data) => async (dispatch) => {
  try {
    await axios.post(`${PORT}/add`, data);
  } catch (error) {
    console.log(error.message);
  }
};
export const LoginUser = (root) => async (dispatch) => {
  try {
    const data = await axios.post(`${PORT}/users`, root);
    console.log(data)
    const logindata = data.data.data._doc;
    dispatch({ type: "LOGINUSER", payload: logindata });
    localStorage.setItem("user", JSON.stringify(logindata));
  } catch (error) {
    console.log(error);
  }
};

export const apiProducts = () => async (dispatch) => {
  try {
    const {data} = await axios.get(`${PORT}/product/all`);
      dispatch({ type: "API_PRODUCTS", payload: data });
  } catch (error) {
    console.log(error.message);
  }
};
export const getProductDetails = (productId) => async (dispatch) => {
  try {
    const data = await axios.get(`${PORT}/product/${productId}`);
    const setdata=data.data.productdetail
    dispatch({type:'PRODUCT_DETAILS',payload:setdata})
  } catch (error) {
    console.log(error);
  }
};
