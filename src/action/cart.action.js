import axios from "axios";
const base = "https://idgxtkil7f.execute-api.ap-south-1.amazonaws.com/dev";

export const getCart = (userId) => async (dispatch) => {
  try {
    const {
      data: { data },
    } = await axios.get(`${base}/cart/${userId}`);
    dispatch({ type: "GET_ITEMS", payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const addToCart =
  ({ userId, productId }) =>
  async (dispatch) => {
    try {
      await axios.post(`${base}/cart/add`, {
        userId,
        productId,
      });
      userId && dispatch(getCart(userId));
    } catch (error) {
      console.log(error.message);
    }
  };

export const updateCart =
  ({ cartId, qty }) =>
  async (dispatch) => {
    try {
      await axios.put(`${base}/cart/update/${cartId}`, { qty });
      const data = JSON.parse(localStorage.getItem("user"));
      dispatch(getCart(data._id));
    } catch (error) {
      console.log(error.message);
    }
  };

export const deleteCart = (cartId) => async (dispatch) => {
  try {
    await axios.delete(`${base}/cart/delete/${cartId}`);
    const data = JSON.parse(localStorage.getItem("user"));
    dispatch(getCart(data._id));
  } catch (error) {
    console.log(error.message);
  }
};
