export const Logoutuser = () => async (dispatch) => {
  try {
    localStorage.removeItem("user");
    dispatch({ type: "LOG_OUT" });
  } catch (error) {
    console.log(error);
  }
};
export const checklogin = () => (dispatch) => {
  let data = JSON.parse(localStorage.getItem("user"));
  data &&  dispatch({ type: "LOGINUSER", payload: data }) 
};
