export const SHOW_SNACK = "SHOW_SNACK";
export const HIDE_SNACK = "HIDE_SNACK";

export const showSnack = (data) => (dispatch) => dispatch({ type: SHOW_SNACK, payload: data });
export const closeSnack = () => (dispatch) => dispatch({ type: HIDE_SNACK });
