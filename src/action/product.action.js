


export const searchdata = (data) => (dispatch) => {
  try {
    console.log(data)
    dispatch({ type: "SEARCH_DATA", payload: data });
  } catch (error) {
    console.log(error.message);
  }
};
