import axios from "axios";
import { showLoader, hideLoader, showSnack } from ".";
import store from "Store/store";

export * from "./cart.action";
export * from "./session.action";
export * from "./apiaction";
export * from "./cart.action";
export * from "./loader.action";
export * from "./snack.action";

axios.interceptors.request.use(
  (req) => {
    store.dispatch(showLoader());
    return req;
  },
  (err) => {
    if (err && err.response && err.response.data) {
      store.dispatch(
        showSnack({ severity: "error", text: err.response.data.error })
      );
    } else {
      store.dispatch(showSnack({ severity: "error", text: err.message }));
    }
    store.dispatch(hideLoader());
    return err;
  }
);
axios.interceptors.response.use(
  (res) => {
    store.dispatch(hideLoader());
    return res;
  },
  (err) => {
    if (err && err.response && err.response.data) {
      store.dispatch(
        showSnack({ severity: "error", text: err.response.data.error })
      );
    } else {
      store.dispatch(showSnack({ severity: "error", text: err.message }));
    }
    store.dispatch(hideLoader());
    return err;
  }
);
