import React, { useEffect } from "react";
import { connect } from "react-redux";
import Routessetup from "Routes/Routessetup";
import { withRouter } from "react-router-dom";
import { apiProducts, getCart } from "action";
import { checklogin } from "action";
import Loader from './components/Loader';
import SnackbarComponent from './components/SnackbarComponent';

function App({ apiProducts, checklogin, getCart }) {
  useEffect(() => {
;
  }, []);

  return (
    <>
    <Loader />
    <SnackbarComponent />
    <Routessetup />
    </>
  );
}

const mapStatetoprops = (state) => ({
  root: state.session.loginuser,
});
const mapDispatchtoprops = (dispatch) => ({
  apiProducts: () => dispatch(apiProducts()),
  checklogin: () => dispatch(checklogin()),
  getCart: (data) => dispatch(getCart(data)),
});
export default withRouter(connect(mapStatetoprops, mapDispatchtoprops)(App));
