export {default as Home} from './Home'
export {default as Login} from './login'
export {default as Product} from './Product/SingleProduct'
export {default as Cart} from './Cart'