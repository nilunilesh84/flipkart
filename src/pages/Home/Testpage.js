import * as React from "react";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";

export default function CircularIntegration() {
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const timer = React.useRef();

  const buttonSx = {
    ...(success && {
      bgcolor: green[500],
      "&:hover": {
        bgcolor: green[700],
      },
    }),
  };

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  const handleButtonClick = () => {
    if (!loading) {
      setSuccess(false);
      setLoading(true);
      timer.current = window.setTimeout(() => {
        setSuccess(true);
        setLoading(false);
      }, 1000);
    }
  };

  return (
    <div style={{ margin: "80px 0px 0px 90px" }}>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ m: 1, position: "relative" }}>
          <Button 
          style={{width:200,paddingLeft:50}}
            variant="contained"
            onClick={handleButtonClick}>
            {loading && (
              <CircularProgress
                size={20}
                sx={{
                  position: "absolute",
                  top: 45,
                  left: 10,
                  zIndex:5,
                }}
              />
            )}
            hello
          </Button>
        </Box>
        <Box sx={{ m: 1, position: "relative" }}>
          <Button
            variant="contained"
            sx={buttonSx}
            disabled={loading}
            onClick={handleButtonClick}
          >
            Accept terms
          </Button>
        </Box>
      </Box>
    </div>
  );
}

// import * as React from 'react';
// import {useRef} from 'react'
// import Button from '@material-ui/core/Button';
// import Menu from '@material-ui/core/Menu';
// import MenuItem from '@material-ui/core/MenuItem';
// import {Popover} from '@material-ui/core'

// export default function Testpage() {
//   const [anchorEl, setAnchorEl] = React.useState(null);
//   const open = Boolean(anchorEl);
//   const handleClick = (event) => {
//     setAnchorEl(event.currentTarget);
//   };
//   const handleClose = () => {
//     setAnchorEl(null);
//   };

//   return (
//     <div>
//       <Button
//         id="basic-button"
//         aria-controls="basic-menu"
//         aria-haspopup="true"
//         aria-expanded={open ? 'true' : undefined}
//         onClick={handleClick}
//       >
//         Dashboard
//       </Button>
//       <Popover
//         id="basic-menu"
//         anchorEl={anchorEl}
//         open={open}
//         // anchorOrigin={{
//         //     vertical: 'bottom',
//         //     horizontal: 'left',
//         //   }}
//         //   transformOrigin={{
//         //     vertical: 'bottom',
//         //     horizontal: 'center',
//         //   }}
//           anchorOrigin={{
//             vertical: 'bottom',
//             horizontal: 'left',
//           }}
//           transformOrigin={{
//             vertical: 'top',
//             horizontal: 'left',
//           }}
//           anchorPosition={{
//               top:200
//           }}
//         onClose={handleClose}
//         MenuListProps={{
//           'aria-labelledby': 'basic-button',
//         }}
//       >
//         <MenuItem onClick={handleClose}>Profile</MenuItem>
//         <MenuItem onClick={handleClose}>My account</MenuItem>
//         <MenuItem onClick={handleClose}>Logout</MenuItem>
//       </Popover>
//     </div>
//   );
// }

// const btn = useRef(null);
// // const btn = document.getElementById('btn');
// if (btn.current) {
//   btn.current.onmouseenter = () => console.log("mouse enter");
//   btn.current.onmouseleave = () => console.log("mouse leave");
// }
