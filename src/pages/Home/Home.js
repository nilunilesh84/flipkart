import React from "react";
import Slide from "./Slide";
import Navbar from "./Navbar";
import Banner from "./Banner";
import Helmet from "react-helmet";
import { Box, makeStyles } from "@material-ui/core";
import Middle from "./Middle";
import { connect } from "react-redux";
import { products } from "../../data/data";
const useStyles = makeStyles((theme) => ({
  slide: {
    width: "81%",
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  },
  addComponent: {
    padding: "auto",
    width: 200,
    paddingLeft: 12,
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
}));
function Home() {
  const alldata = [
    { name: 'nilu', class: '23454' },
    { name: 'guddi', class: '' },
    { name: 'boby', class: '' },
    { name: 'vikky', class: '' },
  ]
  const ansdata = [
    // 'vikky', 'raju', 'nilu', 'nilu'
    { name: 'nilu', class: '98', roll: '12' },
    { name: 'raju', class: '12', roll: '15' },
    { name: 'vikky', class: '45', roll: '14' },
  ]
  // console.log(alldata.filter(ite => !ansdata.includes(ite.name)))

  const mapname = alldata.map(ite=>ite.name)
  const filterf = ansdata.filter(ite=> mapname.includes(ite.name))
  console.log(filterf)
 


  // const books = [
  //   { title: "C++", author: "Bjarne" },
  //   { title: "Java", author: "James" },
  //   { title: "Python", author: "Guido" },
  //   { title: "Java", author: "James" },
  // ];

  // const jsonObject = books.map(JSON.stringify);
  // const uniqueSet = new Set(jsonObject);
  // const uniqueArray = Array.from(uniqueSet).map(JSON.parse);
  // console.log(uniqueArray)

//   import React, { useState } from 'react'

//   function Home() {
//     const [first, setfirst] = useState([
//       // 'vikky', 'raju', 'nilu', 'nilu'
//       { name: 'nilu', class: '98', roll: '12',type:'date' },
//       { name: 'raju', class: '12', roll: '15' },
//       { name: 'vikky', class: '45', roll: '14' },
//     ])
//     const changefirst = (value, index) => {
//       const data = first.map((ite, i) => index === i ? { ...ite, answer: value } : ite)
//       setfirst(data)
//     }
//     console.log(first)
//     return (
//       <div style={{ alignSelf: 'center', marginTop: 130 }}>
//         {
//           first.map((item, i) => (
//             <div>
//               {item.name}
//               <input type='text' onChange={(e) => changefirst(e.target.value, i)} />
//             </div>
//           ))
//         }
//       </div>
//     )
//   }
  
//   export default Home


















  const classes = useStyles();
  const adURL =
    "https://rukminim1.flixcart.com/flap/464/708/image/633789f7def60050.jpg?q=70";
  return (
    <>
      <Helmet>
        <title>Online shoping sites for mobile</title>
      </Helmet>
      <Box>
        <Navbar />
        <Banner />
        <Box style={{ display: "flex" }}>
          <div className={classes.slide}>
            <Slide
              products={products}
              timer={true}
              button={true}
              title="Deals of the Day"
            />
          </div>
          <img src={adURL} alt="" className={classes.addComponent} />
        </Box>
        <Middle />
        <Slide
          products={products}
          timer={true}
          button={true}
          title="  Deals for you"
        />
        <Slide
          products={products}
          timer={true}
          button={true}
          title=" Todays Discounts"
        />
      </Box>
    </>
  );
}
const mapStatetoprops = (state) => ({
  products: state.product.data,
});

export default connect(mapStatetoprops, null)(Home);
