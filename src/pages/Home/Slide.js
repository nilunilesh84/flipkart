import React from "react";
import Carousel from "react-multi-carousel";
import {
  makeStyles,
  Box,
  Typography,
  Divider,
  Button,
} from "@material-ui/core";
import Countdown from "react-countdown";
import { Link } from "react-router-dom";
import "react-multi-carousel/lib/styles.css";

const useStyles = makeStyles((theme) => ({
  component: {
    marginTop: 10,
    paddingLeft: 10,
    [theme.breakpoints.down("sm")]: {
      objectFit: "cover",
      height: 180,
      paddingBottom: 120,
    },
  },
  image: {
    height: 150,
    paddingTop: 10,
    paddingLeft: 32,
    cursor: "pointer",
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      objectFit: "cover",
      height: 100,
    },
  },
  btn: {
    marginLeft: "auto",
  },
  font1: {
    fontWeight: 500,
    textAlign: "center",
  },
  font2: {
    fontWeight: 400,
    color: "#388E3C",
    textAlign: "center",
  },
  font3: {
    fontWeight: 300,
    fontSize: 15,
    textAlign: "center",
  },
  timer: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
}));

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};
const Slide = ({ timer, title, button, products }) => {
  const classes = useStyles();

  const timerURL =
    "https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/timer_a73398.svg";
  return (
    <Box className={classes.component}>
      <Box style={{ display: "flex" }}>
        <Typography style={{ margin: 3, fontWeight: 500 }} variant="h6">
          {title}
        </Typography>
        {timer && (
          <div className={classes.timer}>
            <img src={timerURL} alt="" />
            <Countdown date={Date.now() + 3600000} />
          </div>
        )}
        {button && (
          <Button className={classes.btn} variant="contained" color="primary">
            View All
          </Button>
        )}
      </Box>
      <Divider />
      <Carousel
        responsive={responsive}
        infinite={true}
        draggable={false}
        swipeable={false}
        centerMode={true}
        autoPlay={true}
        autoPlaySpeed={4000}
        keyBoardControl={true}
        // removeArrowOnDeviceType={["tablet", "mobile"]}
      >
        {products?.map((item, i) => (
          <Link
            key={i}
            style={{ textDecoration: "none", color: "inherit" }}
            to={`/product/${item.id}`}
          >
            <img src={item.url} alt="" className={classes.image} />
            <Typography className={classes.font1}>
              {item.title.shortTitle}
            </Typography>
            <Typography className={classes.font2}>
              Min
              {item.price.discount}
            </Typography>
            <Typography className={classes.font3}>{item.tagline}</Typography>
          </Link>
        ))}
      </Carousel>
    </Box>
  );
};

export default Slide;
