import React from "react";
import Carousel from "react-material-ui-carousel";
import { bannerData } from "../../data/data";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme=>({
  image: {
    width: "100%",
    height: 250,
    [theme.breakpoints.down('sm')]:{
      objectFit:'cover',
      height:180
    }
  },
}));

const Banner = () => {
  const classes = useStyles();
  return (
    <Carousel>
      {bannerData.map((item, i) => (
        <img src={item} alt="" className={classes.image} />
      ))}
    </Carousel>
  );
};

export default (Banner);
