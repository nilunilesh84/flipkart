import React from "react";
import { navData } from "../../data/data";
import { Box, Typography, makeStyles, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  component: {
    display: "flex",
    marginTop: "60px",
    alignItems:'center',
    [theme.breakpoints.down("md")]: {
      margin: 0,
    },
  },
  container: {
    display: "flex",
    margin:'auto',
    flexDirection: "column",
    alignItems:'center',
    "&:hover": {
      color: "#2874f0",
    },
    [theme.breakpoints.down("md")]: {
      padding: 0,
      marginTop: 40,
    },
  },
  image: {
    width: 90,
    cursor: "pointer",
    [theme.breakpoints.down("md")]: {
      width: 60,
    },
  },
  typography: {
    cursor: "pointer",
    fontWeight: 500,
    [theme.breakpoints.down("md")]: {
      fontSize:15, fontWeight: 400,
    },
  },
}));

const Navbar = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.component}>
      {navData.map((item) => (
        <Grid item md={1} sm={4}  className={classes.container}>
          <img src={item.url} alt="" className={classes.image} />
          <Typography className={classes.typography}>{item.text}</Typography>
        </Grid>
      ))}
    </Grid>
  );
};

export default Navbar;
