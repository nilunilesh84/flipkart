import React from "react";
import { ImageURL } from "../../data/data";
import { Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  image: {
    height: 245,
    width:'100%',
    cursor: "pointer",
    [theme.breakpoints.down("md")]: {
      paddingTop: 5,
      height: 220,
      margin:'auto'
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: 5,
      height: 180,
    },
  },
}));

function Middle() {
  const classes = useStyles();
  return (
    <Grid
      container
      style={{
        display: "flex",
        justifyContent: "space-between",
      }}
    >
      {ImageURL.map((item, i) => (
        <Grid item md={4} sm={10} xs={12}>
          <img src={item} key={i} className={classes.image} alt="" />
        </Grid>
      ))}
    </Grid>
  );
}

export default Middle;
