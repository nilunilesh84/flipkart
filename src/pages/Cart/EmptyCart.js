import React from "react";
import { Typography, makeStyles, Button } from "@material-ui/core";
import { Link } from "react-router-dom";

const usestyles = makeStyles({
  wrapper: {
    width: "90%",
    height: "75vh",
    margin: "0px 40px 0px 48px",
  },
  typo: {
    fontSize: 20,
    padding: "13px 0px 13px 23px",
    fontWeight: 500,
  },
  btntextWrapper:{
      display:'flex',
      flexDirection:'column',
      alignItems:'center',
      '& > *':{
          margin:'10px 0px 0px 0px'
      }
  },
  loginbtn: {
    width: 200,
    textTransform: "none",
    color: "white",
    border: "none",
    fontSize: 15,
    borderRadius:0,
    fontWeight: 550,
  },
  img:{
      marginTop:60,
      width:230
  }
});
const EmptyCard = () => {
  const emptycarturl =
    "https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90";

  const classes = usestyles();
  return (
    <div className={classes.wrapper}>
      <Typography className={classes.typo}>My Cart</Typography>
      <div className={classes.btntextWrapper}>
      <img className={classes.img} alt="" src={emptycarturl}/>
      <Typography className={classes}>Your cart is empty</Typography>
      <Typography className={classes}>
       Add items to it now
      </Typography>
     <Link style={{textDecoration:'none'}} to='/'>
     <Button
        style={{ background: "#2874F0" }}
        className={classes.loginbtn}
      >
        Shop now
      </Button></Link>
      </div>
    </div>
  );
};

export default EmptyCard;
