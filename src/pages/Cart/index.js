export {default} from './Cart'
// import React, { useEffect } from "react";
// import { Grid } from "@material-ui/core";
// import { useDispatch, useSelector } from "react-redux";
// import { getCart, updateCart } from "action";

// function Cart() {
//   const dispatch = useDispatch();
//   const { cart } = useSelector((state) => state.cart);
//   const userdata = localStorage.getItem("user");

//   const getPriceDetails = () => {
//     let price = 0;
//     cart.forEach((element) => {
//       price = price + element.qty * element.productId.price.mrp;
//     });
//     return price;
//   };
//   console.log(cart)
//   useEffect(() => {
//     dispatch(getCart(JSON.parse(userdata)._id));
//     console.log('hello')
//   }, []);
//   return (
//     <Grid container spacing={3} style={{ paddingTop: 100 }}>
//       <Grid item sm={8} xs={12}>
//         {cart &&
//           cart.map((item) => (
//             <div key={item._id}>
//               <input
//                 type="number"
//                 onChange={(e) => {
//                   const obj = {
//                     cartId: item._id,
//                     qty: parseInt(e.target.value),
//                   };
//                   dispatch(updateCart(obj));
//                 }}
//                 value={item.qty}
//               />
//             </div>
//           ))}
//       </Grid>
//       <Grid item sm={4} xs={12}>
//         {cart && getPriceDetails()}
//       </Grid>
//     </Grid>
//   );
// }

// export default Cart;
