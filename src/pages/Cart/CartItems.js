import React from "react";
import { Button, Card, Grid, makeStyles, Typography } from "@material-ui/core";
import { deleteCart, updateCart } from "action";

import { useDispatch } from "react-redux";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    borderRadius: 0,
    borderTop: "2px light gray",
  },
  wrapper2: {
    marginTop: 30,
  },
  button: {
    borderRadius: "50%",
  },
  leftbox: {
    margin: 20,
    display: "flex",
    flexDirection: "column",
  },
  rightbox: {
    margin: 20,
  },
  smalltext: {
    fontSize: 14,
  },
  mediumText: {
    color: "#878787",
  },
  price: {
    fontSize: 18,
    fontWeight: 500,
  },
  img: {
    height: 109,
    width: 100,
    marginRight: 29,
    marginLeft: 15,
    [theme.breakpoints.down("md")]: {
      width: 80,
      height: 88,
      margin:'10px 5px 5px 59px'
    },
  },
}));

const CartItems = ({ item }) => {
  const dispatch = useDispatch();
  const fassured =
    "https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png";
  const classes = useStyles();
  const deleteItems = (item) => {
    dispatch(deleteCart(item._id));
  };
  return (
    <>
      <Grid container className={classes.wrapper}>
        <Grid item md={3} sm={10} xs={11} className={classes.leftbox}>
          <img src={item.productId.url} alt="" className={classes.img} />
          <input
            type="number"
            onChange={(e) => {
              const obj = {
                cartId: item._id,
                qty: parseInt(e.target.value),
              };
              dispatch(updateCart(obj));
            }}
            value={item.qty}
          />
        </Grid>
        <Grid item md={7} sm={10} xs={11} className={classes.rightbox}>
          <Typography>{item.productId.title.longTitle}</Typography>
          <Typography>
            Seller : OmniTech
            <span>
              <img
                src={fassured}
                alt=""
                style={{ width: 50, marginLeft: 10 }}
              />
            </span>
          </Typography>
          <Typography style={{ margin: "20px 0" }}>
            <span className={classes.price}>
              ₹{item.productId.price.cost * item.qty}{" "}
            </span>{" "}
            &nbsp;
            <span>
              <strike>₹{item.productId.price.mrp}</strike>
            </span>
            &nbsp;&nbsp;
            <span style={{ color: "#388F#C" }}>
              {item.productId.price.discount} off
            </span>
          </Typography>
          <Button onClick={() => deleteItems(item)} style={{ marginLeft: 53 }}>
            Remove
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default CartItems;
