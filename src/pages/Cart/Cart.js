import { getCart } from "action";
import React, { useEffect } from "react";
import { makeStyles, Typography, Button, Grid } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import Price from "./Price";
import CartItems from "./CartItems";
import EmptyCard from "./EmptyCart";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    marginTop: 55,
    padding: "20px 0px 0px 70px",
    [theme.breakpoints.down("md")]: {
      marginTop: 115,
      padding: 0,
    },
  },
  loginbtn: {
    color: "white",
    width: 250,
    height: 45,
    marginLeft: "auto",
    borderRadius: 0,
    fontSize: 15,
    fontWeight: 550,
    [theme.breakpoints.down("md")]: {
      width: 150,
      fontSize: 11,height: 35,
    },
  },
  orderwrapper: {
    textAlign: "right",
    padding: "16px 22px",
    background: "#fff",
    borderTop: "3px solid #f0f0f0",
    boxShadow: "6px 10px 12px lightgray",
    marginBottom: 20,
  },
}));
function Cart() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { cart } = useSelector((state) => state.cart);
  const user = JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    user && dispatch(getCart(user._id));
  }, []);
  return (
    <>
      {cart.length ? (
        <Grid container className={classes.wrapper}>
          <Grid item md={8} sm={10} xs={12}>
            <Typography
              className={classes.header}
              style={{
                fontWeight: 500,
                fontSize: 19,
                marginLeft: 15,
              }}
            >
              My cart({cart.length})
            </Typography>
            {cart &&
              cart.map((item) => (
                <div key={item._id}>
                  <CartItems item={item} />
                </div>
              ))}
            <div className={classes.orderwrapper}>
              <Button
                className={classes.loginbtn}
                style={{ background: "#FB641B" }}
                variant="contained"
              >
                Place Order
              </Button>
            </div>
          </Grid>
          <Grid item md={3} sm={10} xs={12}>
            <Price cart={cart} />
          </Grid>
        </Grid>
      ) : (
        <EmptyCard />
      )}
    </>
  );
}

export default Cart;
