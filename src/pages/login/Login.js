import {
  Dialog,
  makeStyles,
  Box,
  Typography,
  TextField,
  InputAdornment,
  withStyles,
  Button,
  Grid,
  Hidden,
} from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import React, { useState } from "react";
import { BackImage } from "../../data/data";
import * as yup from "yup";
import { useFormik } from "formik";
import { LoginUser } from "action/apiaction";
import { connect } from "react-redux";
import Register from "./Register";

const validationSchema = yup.object({
  email: yup
    .string("must be string")
    .min(8, "must be 8 digit")
    .required("must required"),
  password: yup
    .string("must be string")
    .min(8, "must be 8 digit")
    .required("must be required"),
});

const useStyles = makeStyles((theme) => ({
  component: {
    height: "90vh",
    width: "100%",
    [theme.breakpoints.down("md")]: {
      width: "auto",
    },
    display: "flex",
  },
  typography: {
    color: "white",
    padding: "40px 0 0 30px",
    fontWeight: 500,
  },
  typography2: {
    color: "white",
    fontSize: 20,
    padding: "40px 20px 0 30px",
  },
  image: {
    backgroundImage: `url(${BackImage})`,
    height: "100%",
    backgroundRepeat: "no-repeat",
    background: "#2874f0",
    width: 300,
    backgroundPosition: "center 85%",
  },
  wrapper2: {
    width: 380,
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    padding: "10px 35px 10px 30px",
    "& > *": {
      padding: "15px 0px 12px 10px",
    },
  },
  forgotText: {
    cursor: "pointer",
  },
  policy: {
    color: "#969696",
    fontWeight: 450,
  },
  requestbtn: {
    background: "#FFFFFF",
    textTransform: "none",
    color: "#2874f0",
    border: "none",
    boxShadow: "2px 5px 8px lightgray",
    fontSize: 15,
    fontWeight: 550,
    cursor: "pointer",
  },
  loginbtn: {
    textTransform: "none",
    cursor: "pointer",
    color: "white",
    border: "none",
    fontSize: 15,
    fontWeight: 550,
  },
  createtext: {
    color: "#2874f0",
    fontWeight: 500,
    textAlign: "center",
    cursor: "pointer",
    marginTop: "auto",
  },
  dialog:{
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  }
}));
const styles = {
  dialogPaper: {
    width: "50%",
    height: "89%",
    borderRadius: 0,
    boxShadow: "none",
    maxHeight: "100%",
    maxWidth: "100%",
  },
};
const initialvalue = {
  login: {
    view: "login",
    heading: "Login",
    Subheading: "Get access to your Orders,Wishlist and Recomdations",
  },
  signup: {
    view: "signup",
    heading: "Looks like you're new here!",
    Subheading: "Sign up with your mobile number to get started",
  },
};
function Login({ open, handleClose, classes, logindatasend }) {
  const classs = useStyles();
  const [openlogin, setopenlogin] = useState(initialvalue.login);
  const handletoggle = () => {
    setopenlogin(initialvalue.signup);
  };
  const handleback = () => {
    setopenlogin(initialvalue.login);
  };
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema,
    onSubmit: (values) => {
      logindatasend(values);
    },
  });
  const [loading, setLoading] = React.useState(false);
  const timer = React.useRef();

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);
  const handleButtonClick = () => {
    handleClose();
    if (!loading) {
      setLoading(true);
      timer.current = window.setTimeout(() => {
        setLoading(false);
      }, 1500);
    }
  };
  return (
    <Grid container>
      <Grid item md={6} sm={9} xs={11}>
        <Dialog
          open={open}
          onClose={handleClose}
          className={classes.dialog}
          classes={{ paper: classes.dialogPaper }}
        >
          <div className={classs.component}>
            <Hidden mdDown>
              <Box className={classs.image}>
                <Typography variant="h5" className={classs.typography}>
                  {openlogin.heading}
                </Typography>
                <Typography className={classs.typography2}>
                  {openlogin.Subheading}
                </Typography>
              </Box>
            </Hidden>
            {openlogin.view === "login" ? (
              <form onSubmit={formik.handleSubmit} className={classs.wrapper2}>
                <TextField
                  fullWidth
                  variant="standard"
                  label="Enter Email"
                  name="email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  error={formik.touched && formik.errors.email}
                  helperText={formik.touched && formik.errors.email}
                />
                <TextField
                  InputProps={{
                    endAdornment: (
                      <InputAdornment style={{ cursor: "pointer" }}>
                        <Typography
                          style={{ color: "#2874f0", fontWeight: 500 }}
                        >
                          Forgot?
                        </Typography>
                      </InputAdornment>
                    ),
                  }}
                  fullWidth
                  variant="standard"
                  name="password"
                  label="Enter Password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  error={formik.touched && formik.errors.password}
                  helperText={formik.touched && formik.errors.password}
                />
                <Typography className={classs.policy}>
                  By continuing,you agree to Flipkart's Terms of Use and Privacy
                  Policy
                </Typography>
                <Button
                  style={{
                    background: "#FB641B",
                  }}
                  className={classs.loginbtn}
                  type="submit"
                  onClick={handleButtonClick}
                >
                  {" "}
                  {loading && (
                    <CircularProgress
                      size={20}
                      sx={{
                        position: "absolute",
                        top: 45,
                        left: 10,
                        zIndex: 5,
                      }}
                    />
                  )}
                  Login
                </Button>
                <Typography
                  style={{
                    color: "#969696",
                    fontWeight: 450,
                    textAlign: "center",
                  }}
                >
                  OR
                </Typography>
                <Button className={classs.requestbtn}>Request OTP</Button>
                <Typography
                  onClick={() => handletoggle()}
                  className={classs.createtext}
                >
                  New to Flipkart ? Create an account
                </Typography>
              </form>
            ) : (
              <Register handleClose={handleClose} handleback={handleback} />
            )}
          </div>
        </Dialog>
      </Grid>
    </Grid>
  );
}
const mapStatetoprops = (state) => ({
  logindatasend: state,
});
const mapDispatchtopprops = (dispatch) => ({
  logindatasend: (data) => dispatch(LoginUser(data)),
});
export default connect(
  mapStatetoprops,
  mapDispatchtopprops
)(withStyles(styles)(Login));
