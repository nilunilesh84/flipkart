import React from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import { Button, TextField, Typography, makeStyles } from "@material-ui/core";
import { connect } from "react-redux";
import { addUsers } from "action";

const useStyles = makeStyles({
  wrapper2: {
    width: 380,
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    padding: "2px 35px 10px 30px",
    "& > *": {
      padding: "9px 0px 12px 10px",
    },
  },
  policy: {
    color: "#969696",
    fontWeight: 450,
  },
  requestbtn: {
    background: "#FFFFFF",
    textTransform: "none",
    color: "#2874f0",
    border:'none',
    boxShadow:'2px 5px 8px lightgray',
    fontSize:15,
    fontWeight: 550,
    cursor:'pointer',
    marginTop:6
  },
  loginbtn: {
    textTransform: "none",
    cursor:'pointer',
    color:'white',
    border:'none',
    fontSize:15,
    fontWeight: 550,
  },
});

const validationSchema = yup.object({
  name: yup
    .string("must be string")
    .min(4, "must be 4 digit")
    .required("must required"),
  email: yup
    .string("must be string")
    .min(8, "must be 8 digit")
    .required("must required"),
  phone: yup
    .string("must be string")
    .min(10,"must be 10 digit")
    .required("must required"),
  password: yup
    .string("must be string")
    .min(8, "must be 8 digit")
    .required("must be required"),
});

function Register({ Registeruser, handleback,handleClose }) {
  const classes = useStyles();
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      phone: "",
      password: "",
    },
    validationSchema,
    onSubmit: (values) => {
      Registeruser(values);
    },
  });
  return (
    <form onSubmit={formik.handleSubmit} className={classes.wrapper2}>
      <TextField
        variant="standard"
        value={formik.values.name}
        name="name"
        label="enter name"
        onChange={formik.handleChange}
        error={formik.touched && formik.errors.name}
        helperText={formik.touched && formik.errors.name}
      />
      <TextField
        fullWidth
        variant="standard"
        name="email"
        label="Enter Email"
        value={formik.values.email}
        onChange={formik.handleChange}
        error={formik.touched && formik.errors.email}
        helperText={formik.touched && formik.errors.email}
      />
      <TextField
        variant="standard"
        value={formik.values.phone}
        name="phone"
        label="enter phone"
        onChange={formik.handleChange}
        error={formik.touched && formik.errors.phone}
        helperText={formik.touched && formik.errors.phone}
      />
      <TextField
        fullWidth
        variant="standard"
        name="password"
        label="Enter Password"
        value={formik.values.password}
        onChange={formik.handleChange}
        error={formik.touched && formik.errors.password}
        helperText={formik.touched && formik.errors.password}
      />
      <Typography className={classes.policy}>
        By continuing,you agree to Flipkart's Terms of Use and Privacy Policy
      </Typography>
      <Button
      style={{
        background: "#FB641B"}}
        className={classes.loginbtn}
        type="submit"
        onClick={handleClose}
      >
        Continue
      </Button>
      <Button
        onClick={() => handleback()}
        className={classes.requestbtn}
      >
        Existing User ? Log in
      </Button>
    </form>
  );
}
const mapStatetoprops = (state) => ({
  root: state.session.user,
});
const mapDispatchtoprops = (dispatch) => ({
  Registeruser: (data) => dispatch(addUsers(data)),
});

export default connect(mapStatetoprops, mapDispatchtoprops)(Register);
