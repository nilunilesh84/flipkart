const products = [
  {
    id: '1',
    name: "Stylish shoe",
    media:
      "https://th.bing.com/th/id/OIP.EnJE7oUVjTa18saP3vcbVgHaHa?w=159&h=180&c=7&o=5&dpr=1.25&pid=1.7",
    qty: 1,
    price: 900,
    desc: "it is formal shoe",
  },
  {
    id: '2',
    name: "Formal",
    media:
      "https://th.bing.com/th/id/OIP.cDx4e04W834TgK7gob-YcQHaHa?w=159&h=180&c=7&o=5&dpr=1.25&pid=1.7",
    qty: 1,
    price: 1000,
    desc: "it is good shoe",
  },
  {
    id: '3',
    name: "Slipers",
    media:'https://th.bing.com/th/id/OIP.cut5vEb4f6mBG1ICQUzmlgHaHa?w=183&h=183&c=7&o=5&dpr=1.25&pid=1.7',
    qty:1,
    price: 1100,
    desc: "it is stylish shoe",
  },
  {
    id: '4',
    name: "Sandles",
    media:
      'https://th.bing.com/th/id/OIP.aqlZPRWduPFJAitypb_vUAHaHa?w=183&h=183&c=7&o=5&dpr=1.25&pid=1.7',
    qty: 1,
    price: 1200,
    desc: "it is sport shoe",
  },
];
export default products;
