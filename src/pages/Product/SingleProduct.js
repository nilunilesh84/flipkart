import React, { useEffect, useState } from "react";
import { makeStyles, Typography, Button, Grid } from "@material-ui/core";
import { addToCart, getProductDetails, getCart } from "action";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import FlashOnIcon from "@material-ui/icons/FlashOn";
import { connect } from "react-redux";
import Error from "../login/Error";
import { products } from "data/data";
const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    width: "100%",
    height: "100vh",
    margin: "100px 0 0px 90px",
    [theme.breakpoints.down("md")]: {
      height: "100vh",
      margin: 0,
    },
  },
  leftBox: {
    width: "34%",
    [theme.breakpoints.down("md")]: {
      width: "auto",
      padding: "auto",
    },
  },
  btns: {
    display: "flex",
    "& > *": {
      cursor: "pointer",
      marginRight: 10,
      width: 300,
      fontWeight: 600,
      fontSize: 15,
      border: "none",
      color: "white",
      height: 40,
      [theme.breakpoints.down("md")]: {
        margin: "10px 5px auto 6px",
        height: 33,
        width: 350,
        fontSize: 11,
      },
    },
  },
  rightBox: {
    margin: "50px 0px 0px 12px",
    width: "66%",
    [theme.breakpoints.down("md")]: {
      width: "auto",
      margin: "1px 0px 0px 12px",
    },
  },
  rightBoxTypography: {
    fontSize: 22,
    fontWeight: 500,
    [theme.breakpoints.down("md")]: {
      fontSize: 18,
    },
  },
  pricewrapper: {
    "& > * ": {
      padding: "15px 0 5px 17px",
    },
  },
  image: {
    width: "80%",
    cursor: "pointer",
    [theme.breakpoints.down("md")]: {
      width: "70%",
      margin: "50px 10px 0 60px",
      paddingBottom: 20,
    },
  },
  price: {
    fontSize: 28,
    fontWeight: 500,
    [theme.breakpoints.down("md")]: {
      fontSize: 20,
    },
  },
  offferwrapper: {
    display: "flex",
    "& > *": {
      margin: "20px 0 5px 10px",
    },
  },
  offerTypo: {
    fontSize: 20,
    fontWeight: 500,
    [theme.breakpoints.down("md")]: {
      fontSize: 15,
    },
  },
}));

function SingleProduct({
  match,
  addtocart,
  loginuser,
  reducerproduct,
  getProductDetails,
}) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClicks = () => {
    setOpen(true);
  };
  const [state, setstate] = useState("ADD TO CART");
  const handleClick = (item) => {
    setstate("GO TO CART");
    !loginuser
      ? handleClicks()
      : addtocart({ userId: loginuser._id, productId: item._id });
  };
  useEffect(() => {
    getProductDetails(match.params.productid);
    const filterdata = products.filter(item => item.id === match.params.productid)
    console.log(filterdata,'d')
  }, [loginuser]);
  return (
    <>
      {products.filter(item => item.id === match.params.productid).map((item, i) => (
        <Grid container className={classes.wrapper} key={i}>
          <Grid item md={3} sm={10} xs={11} className={classes.leftBox}>
            <img alt="" src={item.url} className={classes.image} />
            <Error open={open} setOpen={setOpen} />
            <div className={classes.btns}>
              <Button
                onClick={() => handleClick(item)}
                style={{ background: "#FF9F00" }}
              >
                <ShoppingCartIcon /> {state}
              </Button>
              <Button onClick={handleClicks} style={{ background: "#FB641B" }}>
                <FlashOnIcon /> BUY NOW
              </Button>
            </div>
          </Grid>
          <Grid item md={8} sm={10} xs={11} className={classes.rightBox}>
            <Typography className={classes.rightBoxTypography}>
              {item.title.longTitle}
            </Typography>
            <Typography style={{ fontSize: 20, color: "#388E3C" }}>
              Extra ₹ 200 off
            </Typography>
            <div>
              <Typography style={{ margin: "20px 0" }}>
                <span className={classes.price}>₹ {item.price.cost}</span>{" "}
                &nbsp;
                <span>
                  <strike style={{ fontSize: 20 }}>₹ {item.price.mrp}</strike>
                </span>
                &nbsp;&nbsp;
                <span style={{ color: "#388E3C" }}>
                  {item.price.discount} off
                </span>
              </Typography>
            </div>
            <>
              <Typography className={classes.offerTypo}>Available offers</Typography>
              <div style={{ margin: "10px 0px 5px 20px" }}>
                <Typography>
                  Bank Offer5% Unlimited Cashback on Flipkart Axis Bank Credit
                  CardT&C
                </Typography>
                <Typography>
                  Bank Offer15% Instant discount on first Pay Later order of
                  ₹500 and aboveT&C
                </Typography>
              </div>
            </>
          </Grid>
        </Grid>
      ))}
    </>
  );
}
const mapStateToProps = (state) => ({
  reducerproduct: state.product,
  loginuser: state.session.loginuser,
});
const mapDispatchToProps = (dispatch) => ({
  addtocart: (data) => dispatch(addToCart(data)),
  getProductDetails: (data) => dispatch(getProductDetails(data)),
  getCart: (data) => dispatch(getCart(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);
