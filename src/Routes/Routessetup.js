import React from 'react'
import {Switch,Route} from 'react-router-dom'
import Layout from '../Layout/Layout'
import routes from './Routes';

function Routessetup() {
    return (
       <Layout>
           <Switch>
            {
                routes.map((item,i)=> <Route {...item} key={i}/>)
            }
           </Switch>
    </Layout>
    )
}

export default Routessetup
