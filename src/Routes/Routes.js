import Testpage from "pages/Home/Testpage";
import LoginMobile from "pages/login/LoginMobile";
import { Home, Product, Cart} from "../pages";

const routes = [
  {
    path: "/",
    component: Home,
    exact: true,
  },
  {
    path: "/login",
    component: LoginMobile,
    exact: true,
  },
  {
    path: "/product/:productid",
    component: Product,
    exact: true,
  },
  {
    path: "/cart",
    component: Cart,
    exact: true,
  },
  {
    path: "/test",
    component: Testpage,
    exact: true,
  },
 
];

export default routes;
