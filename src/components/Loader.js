import React from 'react';
import { Backdrop, CircularProgress, useTheme} from '@material-ui/core'
import { useSelector } from 'react-redux';


function Loader() {
    const theme = useTheme();
    const {loading} = useSelector(state=>state.loading)
    return (
        <Backdrop
        open={loading}
        style={{ zIndex: theme.zIndex.drawer+1}}
        >
        <CircularProgress style={{ color: 'white'}} />
        </Backdrop>
    )
}

export default Loader
