import React from "react";
import { Snackbar } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { useDispatch, useSelector } from "react-redux";
import { closeSnack } from 'action'

function SnackbarComponent() {
    const dispatch = useDispatch()
  const { open, severity, text } = useSelector((state) => state.snack);
  console.log(severity)
  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={() => dispatch(closeSnack())}>
      <Alert variant="filled" severity={severity}>
        {text}
      </Alert>
    </Snackbar>
  );
}

export default SnackbarComponent;
