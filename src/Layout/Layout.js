import React, { useState } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  makeStyles,
  Box,
  Grid,
  Hidden,
  Drawer,
  List,
  ListItem,
} from "@material-ui/core";
import { Menu } from "@material-ui/icons";
import SearchBar from "./SearchBar";
import { useSelector,useDispatch } from "react-redux";
import HeaderButton from "./HeaderButton";
import { Link } from "react-router-dom";
import { Logoutuser } from "action";

const useStyles = makeStyles((theme) => ({
  header: {
    background: "#2874f0",
    height: 60,
    [theme.breakpoints.down("md")]: {
      height: 90,
    },
    textDecoration: "none",
  },
  logo: {
    width: 75,
    [theme.breakpoints.down("md")]: {
      width: 55,
    },
  },
  subURL: {
    width: 10,
    marginLeft: 4,
  },
  container: {
    display: "flex",
    marginRight: 5,
    "& > * ": {
      fontSize: 10,
      cursor: "pointer",
      [theme.breakpoints.down("md")]: {
        marginRight: 0,
      },
    },
  },
  component: {
    marginLeft: "12%",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    textDecoration: "none",
    color: "white",
    [theme.breakpoints.down("md")]: {
      margin: "9px auto 0 10px",
    },
  },
  menu: {
    color: "white",
    cursor: "pointer",
    [theme.breakpoints.down("md")]: {
      margin: "9px 0 0 2px",
    },
  },
  listitem: {
    "& > *": {
      fontWeight: 500,
      cursor: "pointer",
    },
  },
}));

const Layout = ({ children }) => {
  const [open, setopen] = useState(null);
const dispatch=useDispatch()
  const { loginuser } = useSelector((state) => state.session);
  const [OpenDrawer, setOpenDrawer] = useState(null);
  const handleClose = () => {
    setOpenDrawer(false);
  };
  const handleLogin = (e) => {
    setopen(e.currentTarget);
    setOpenDrawer(false);
  };
  const logout=()=>{
    dispatch(Logoutuser())
    setOpenDrawer(false);
  }
  const classes = useStyles();
  const logoURL =
    "https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png";
  const subURL =
    "https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png";
  return (
    <Box style={{ marginTop: 50 }}>
      <AppBar className={classes.header}>
        <Drawer open={OpenDrawer} onClose={handleClose}>
          <List className={classes.listitem}>
            {loginuser ? (
              <ListItem>{loginuser.name}</ListItem>
            ) : (
              <Link
                to="/login"
                onClick={handleClose}
                style={{ textDecoration: "none", color: "inherit" }}
              >

                <ListItem>Login</ListItem>
              </Link>
            )}
            <ListItem>SuperCoin Zone</ListItem>
            <ListItem>All Categories</ListItem>
            <ListItem>Offer Zone</ListItem>
            <ListItem>My orders</ListItem>
            <Link
              to="/cart"
              style={{ textDecoration: "none", color: "inherit" }}
            >
              <ListItem onClick={handleClose}>My Cart</ListItem>
            </Link>
            <ListItem>My Wishlist</ListItem>
            <ListItem>My Account</ListItem>
            <ListItem>My Chats</ListItem>
            <ListItem onClick={()=>logout()}>Logout</ListItem>
          </List>
        </Drawer>
        <Toolbar>
          <Grid container>
            <Grid style={{ display: "flex" }} item md={3} sm={8} xs={6}>
              <Hidden smUp>
                <Menu
                  onClick={() => setOpenDrawer(true)}
                  className={classes.menu}
                />
              </Hidden>
              <Link to="/" className={classes.component}>
                <img src={logoURL} alt="" className={classes.logo} />
                <Box className={classes.container}>
                  <Typography>Explore Plus</Typography>
                  <img src={subURL} alt="" className={classes.subURL} />
                </Box>
              </Link>
            </Grid>
            <Grid item md={5} sm={11} xs={12}>
              <SearchBar />
            </Grid>
            <Hidden smDown>
              <Grid item md={4} sm={8} xs={10}>
                <HeaderButton
                  open={open}
                  setopen={setopen}
                  handleLogin={handleLogin}
                />
              </Grid>
            </Hidden>
          </Grid>
        </Toolbar>
      </AppBar>
      {children}
    </Box>
  );
};

export default Layout;
