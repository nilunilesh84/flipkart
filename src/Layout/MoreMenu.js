import * as React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import {  makeStyles, Popover } from "@material-ui/core";

const useStyles = makeStyles({
  componet: {
    width: "50%",
  },
  root2: {
    color: "#2874f0",
  },
});

export default function MoreMenu({ moreMenu, handleClose }) {
  const classes = useStyles();
  const open = Boolean(moreMenu);
  return (
    <Popover
      id="basic-menu"
      className={classes.componet}
      anchorEl={moreMenu}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      open={open}
      onClose={handleClose}
      MenuListProps={{
        "aria-labelledby": "basic-button",
      }}
    >
      <MenuItem onClick={handleClose}>Notification</MenuItem>
      <MenuItem onClick={handleClose}>Sell on flipkart</MenuItem>
      <MenuItem onClick={handleClose}>24*7 Customer care</MenuItem>
      <MenuItem onClick={handleClose}>Advertise</MenuItem>
      <MenuItem onClick={handleClose}>Download App</MenuItem>
    </Popover>
  );
}
