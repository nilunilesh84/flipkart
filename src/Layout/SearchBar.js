import React, { useEffect, useState } from "react";
import { makeStyles, List, ListItem } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { useDispatch, useSelector } from "react-redux";
import { searchdata } from "action/product.action";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
    padding: 3,
    marginLeft: 10,
    borderRadius: 3,
    width: 550,
    [theme.breakpoints.down("md")]: {
      width: 'auto',
      marginLeft: 0,
      marginTop:12,
      margin:'auto'
    },
  },
  input: {
    border: "none",
    paddingLeft: 15,
    width: "100%",
    fontSize: 15,
    "&:focus": {
      outline: "none",
    },
  },
  btn: {
    border: "none",
    color: "#2874f0",
    background: "transparent",
    cursor: "pointer",
  },
  list: {
    position: "absolute",
    color: "black",
    background: "white",
    fontWeight: 600,
    margin: "25px 6px 0px 0px",
    cursor: "pointer",
  },
}));

function SearchBar() {
  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.product);
  const classes = useStyles();
  const [open, setopen] = useState(true);
  const [input, setinput] = useState("");
  const handleChange = (text) => {
    setinput(text);
    setopen(false);
  };
  const clearText = () => {
    setopen(true);
    setinput("");
  };
  useEffect(() => {
    dispatch(searchdata(input));
  }, [dispatch]);
  return (
    <div className={classes.root}>
      <input
        type="text"
        placeholder="search for products,brand and more"
        value={input}
        onChange={(e) => handleChange(e.target.value)}
        className={classes.input}
      />
      <button onClick={() => searchdata(input)} className={classes.btn}>
        <SearchIcon />
      </button>
      {input && (
        <List className={classes.list} hidden={open}>
          {data
            .filter((item) =>
              item.title.longTitle.toLowerCase().includes(input.toLowerCase())
            )
            .map((item) => (
              <ListItem>
                <Link
                  onClick={() => clearText()}
                  style={{ textDecoration: "none", color: "inherit" }}
                  to={`/product/${item._id}`}
                >
                  {item.title.longTitle}
                </Link>{" "}
              </ListItem>
            ))}
        </List>
      )}
    </div>
  );
}

export default SearchBar;
