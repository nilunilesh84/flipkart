import React from "react";
import { Box, Button, makeStyles, Typography, Badge, Hidden } from "@material-ui/core";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import LoginMenu from "./LoginMenu";
import { Link } from "react-router-dom";
import MoreMenu from "./MoreMenu";
import Login from "pages/login";
import { connect } from "react-redux";
import UserMenu from "./UserMenu";
import { Logoutuser,  } from "action";

const useStyles = makeStyles((theme)=>({
  btn: {
    background: "white",
    textTransform: "none",
    fontWeight: 600,
    borderRadius: 2,
    color: "#2874f0",
    padding: "5px 40px",
  },
  usr: {
    textTransform: "none",
    fontWeight: 600,
    color: "white",
    padding: "5px 40px",
  },
  cart: {
    display: "flex",
    color: "white",
    textDecoration: "none",
  },
  wrapper: {
    margin: "0 0 0 40px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& > * ": {
      marginRight: 30,
      fontSize: 15,
      cursor: "pointer",
    },
  },
}));

const HeaderButton = ({ root, logOut, cart,open,setopen,handleLogin }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorEll, setAnchorEll] = React.useState(null);
  const [moreMenu, setMoreMenu] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenu = (event) => {
    setMoreMenu(event.currentTarget);
  };
  const handleUser = (event) => {
    setAnchorEll(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setMoreMenu(null);
    setAnchorEll(null);
    setopen(false);
  };
  const classes = useStyles();
  return (
    <Box className={classes.wrapper}>
      {root ? (
        <>
          <Typography
            variant="h4"
            onMouseOver={(event) => handleUser(event)}
            className={classes.usr}
          >
            {root.name}
          </Typography>
          <UserMenu
            anchorEll={anchorEll}
            handleClose={handleClose}
            logOut={logOut}
          />
        </>
      ) : (
        <Button
          onClick={(e) => handleLogin(e)}
          variant="contained"
          className={classes.btn}
        >
          Login
        </Button>
      )}
      <Login open={open} setopen={setopen} handleClose={handleClose} />
      <LoginMenu anchorEl={anchorEl} handleClose={handleClose} />
     <Hidden mdDown>
     <Typography onMouseOver={(event) => handleMenu(event)}>More</Typography>
      <MoreMenu moreMenu={moreMenu} handleClose={handleClose} />
      <Link to="/cart" className={classes.cart}>
        <Badge badgeContent={cart ? cart.length : null } color="error">
          <ShoppingCart />
        </Badge>
        <Typography style={{ paddingLeft: 4 }}>Cart</Typography>
      </Link>
     </Hidden>
    </Box>
  );
};
const mapStatetoprops = (state) => ({
  root: state.session.loginuser,
  cart: state.cart.cart,
});
const mapDispatchtoprops = (dispatch) => ({
  logOut: () => dispatch(Logoutuser()),
});

export default connect(mapStatetoprops, mapDispatchtoprops)(HeaderButton);
