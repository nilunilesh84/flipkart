import * as React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles, Popover } from "@material-ui/core";
import { connect } from "react-redux";
import { getCart } from "action";

const useStyles = makeStyles({
  componet: {
    width: "50%",
    paddingRight: 500,
  },
  root2: {
    color: "#2874f0",
  },
});

function UserMenu({ anchorEll, handleClose, logOut, removeItem }) {
  const classes = useStyles();
  const open = Boolean(anchorEll);
  const removeandlogout = () => {
    logOut();
    const data = JSON.parse(localStorage.getItem("user"));
    data &&  getCart(data._id);
  };
  return (
    <Popover
      id="basic-menu"
      className={classes.componet}
      anchorEl={anchorEll}
      open={open}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      onClose={handleClose}
      MenuListProps={{
        "aria-labelledby": "basic-button",
      }}
    >
      <MenuItem onClick={handleClose}>My profile</MenuItem>
      <MenuItem onClick={handleClose}>Super coin zone</MenuItem>
      <MenuItem onClick={handleClose}>Flipkart plus zone</MenuItem>
      <MenuItem onClick={handleClose}>Orders</MenuItem>
      <MenuItem onClick={handleClose}>Wishlist</MenuItem>
      <MenuItem onClick={handleClose}>My Chats</MenuItem>
      <MenuItem onClick={handleClose}>Coupons</MenuItem>
      <MenuItem onClick={handleClose}>Gift Cards</MenuItem>
      <MenuItem onClick={handleClose}>Notification</MenuItem>
      <MenuItem onClick={() => removeandlogout()}>Logout</MenuItem>
    </Popover>
  );
}

const mapDispatchToProps = (dispatch) => ({
  getCart: (data) => dispatch(getCart(data)),
});

export default connect(null, mapDispatchToProps)(UserMenu);
