import * as React from "react";
import { Button, Menu, MenuItem } from "@material-ui/core";
import { logOut } from "action/session.action";
import { connect } from "react-redux";

function LogoutMenu({ anchorEl, setAnchorEl, logOut }) {
  const open = Boolean(anchorEl);

  const handleClose = () => {
    setAnchorEl(null);
  };
  const logoutnow = () => {
    handleClose();
    logOut();
  };

  return (
    <Menu
      id="basic-menu"
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      anchorPosition={{
        top: 100,
        left: 400,
      }}
      open={open}
      onClose={handleClose}
      MenuListProps={{
        "aria-labelledby": "basic-button",
      }}
    >
      <MenuItem onClick={handleClose}>Profile</MenuItem>
      <MenuItem onClick={handleClose}>My account</MenuItem>
      <MenuItem onClick={logoutnow}>Logout</MenuItem>
    </Menu>
  );
}
const mapDispatchtoprops = (dispatch) => ({
  logOut: () => dispatch(logOut()),
});
export default connect(null, mapDispatchtoprops)(LogoutMenu);
