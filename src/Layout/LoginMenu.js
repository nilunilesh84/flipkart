import * as React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { Typography, makeStyles, Popover } from "@material-ui/core";

const useStyles = makeStyles({
  componet: {
    width: "50%",
  },
  menu: {
    padding: 4,
    "& > * ": {
      margin: 10,
      fontSize: 15,
      cursor: "pointer",
      fontWeight: 550,
    },
  },
  root2: {
    color: "#2874f0",
  },
});

export default function LoginMenu({ anchorEl, handleClose }) {
  const classes = useStyles();
  const open = Boolean(anchorEl);
  return (
    <Popover
      id="basic-menu"
      className={classes.componet}
      anchorEl={anchorEl}
      open={open}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      onClose={handleClose}
      MenuListProps={{
        "aria-labelledby": "basic-button",
      }}
    >
      <MenuItem className={classes.menu} onClick={handleClose}>
        <Typography>New customer? </Typography>
        <Typography className={classes.root2}>Sign Up </Typography>
      </MenuItem>
      <MenuItem onClick={handleClose}>Profile</MenuItem>
      <MenuItem onClick={handleClose}>My account</MenuItem>
      <MenuItem onClick={handleClose}>Orders</MenuItem>
      <MenuItem onClick={handleClose}>Wishlist</MenuItem>
      <MenuItem onClick={handleClose}>Rewards</MenuItem>
      <MenuItem onClick={handleClose}>Logout</MenuItem>
    </Popover>
  );
}
